Pod::Spec.new do |s|

  s.name        = "AuthenteqFlow"
  s.version     = "1.55.3-swift-5.1"
  s.summary     = "Authenteq Flow"
  s.homepage    = "http://authenteq.com"

  s.description = <<-DESC
        This is a SDK which is include Authenteq functionality
        DESC

  s.license     = {
        :type => 'commercial',
        :text => <<-LICENSE
                © 2013-2019 Authenteq. All rights reserved.
                LICENSE
        }

  s.authors     = {
        "Authenteq" => "info@authenteq.com",
        "Vitaliy Gozhenko" => "vitaliy@authenteq.com"
  }

  s.source      = {
        :git => 'https://AuthenteqSDK@bitbucket.org/authenteq/flow-ios-release.git',
        :tag => s.version
  }

  s.platform     = :ios

  # ――― MULTI-PLATFORM VALUES ――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.swift_version = '5'
  s.ios.deployment_target = '10.0.0'
  s.ios.vendored_frameworks = 'AuthenteqFlow.framework', 'AuthenteqHelperObjC.framework'
  s.dependency 'Alamofire', '~> 4'
  s.dependency 'RxSwift', '~> 5'
  s.dependency 'RxCocoa', '~> 5'
  s.dependency 'ZoomAuthenticationHybrid', '~> 7'
  s.dependency 'PPBlinkID', '~> 4.10.0'
  s.dependency 'R.swift', '~> 5'
end
