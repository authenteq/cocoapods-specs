Pod::Spec.new do |s|

  s.name        = "AuthenteqFlow"
  s.version     = "1.45.4"
  s.summary     = "Authenteq Flow"
  s.homepage    = "http://authenteq.com"

  s.description = <<-DESC
        This is a SDK which is include Authenteq functionality
        DESC

  s.license     = {
        :type => 'commercial',
        :text => <<-LICENSE
                © 2013-2019 Authenteq. All rights reserved.
                LICENSE
        }

  s.authors     = {
        "Authenteq" => "info@authenteq.com",
        "Vitaliy Gozhenko" => "vitaliy@authenteq.com"
  }

  s.source      = {
        :git => 'https://AuthenteqSDK@bitbucket.org/authenteq/flow-ios-release.git',
        :tag => '1.45.4'
  }

  s.platform     = :ios

  # ――― MULTI-PLATFORM VALUES ――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.swift_version = '4.2'
  s.ios.deployment_target = '10.0.0'
  s.ios.vendored_frameworks = 'AuthenteqFlow.framework', 'AuthenteqHelperObjC.framework'
  s.dependency 'Alamofire', '~> 4'
  s.dependency 'RxAlamofire', '~> 4'
  s.dependency 'RxSwift', '~> 4'
  s.dependency 'RxCocoa', '~> 4'
  s.dependency 'SwifterSwift', '~> 4'
  s.dependency 'PPBlinkID', '~> 4.6.0'
end
