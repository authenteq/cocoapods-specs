Pod::Spec.new do |s|

  s.name         = "FaceTecSDK"
  s.version      = "9.6.53"
  s.summary      = "FaceTec's ZoOm iOS SDK - 3D Face Login + TrueLiveness"
  s.homepage     = "https://dev.zoomlogin.com"
  s.license      = { type: 'custom', text: 'FaceTecSDK is Copyright 2023 FaceTec, Inc.  It may not be modified.' }
  s.author       = { "Gregory Perez" => "gperez@facetec.com" }

  s.platform     = :ios, "11.0"

  s.source      = {
        :http => 'https://repo.app.authenteq.com/repository/artifacts/ios/zoom/9.6.53/zoom-9.6.53-xcframework.zip'
  }

  s.framework    = "FaceTecSDK"
  s.ios.deployment_target = '11.0.0'
  s.ios.vendored_frameworks = 'FaceTecSDK.xcframework'

end
